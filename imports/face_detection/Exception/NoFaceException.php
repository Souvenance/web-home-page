<?php

/**
 * Throws exception if face was not detected in `faceDetect` call.
 */

namespace FaceDetector;

use Exception;

class NoFaceException extends Exception {

}
