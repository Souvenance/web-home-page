<?php
/**
* App.php
* @author Souvenance <skavunga@gmail.com>
* @version 1.1
* @importance Les fonctions communs des tout les controllers de l'application
* Tout les controllers doivent heriter cette Classe
*/

    namespace Stephanie\Controller;

	use App\View\AppView;
	use Stephanie\Router\Router;
	use Stephanie\Handlers\Flash;
	use Stephanie\Handlers\Session;
	use Stephanie\Auth\Auth;
	use Cake\ORM\TableRegistry;
	use FaceDetector\FaceDetector;	

	/**
	 * Classe generique de tout les controllers. Tout les controllers doivent
	 * heriter de cette class
	 */
    class Controller {

		protected $view;
		protected static $model;
		protected $flash;
		protected $request;
		protected $session;
		protected $auth;

		private static $className;

    	public function __construct()
    	{
			self::$className = substr(get_class($this), 15, 35);
			$this->view      = new AppView(get_class($this));

			$this::$model    = TableRegistry::get(self::$className, self::ModelClass());
			$this->request   = new \Stephanie\Request();
			$this->session   = new Session();
			$this->flash     = new Flash($this->session);
			$this->auth		 = new Auth();
    	}
		
		/**
		 * Page d'accueil par defaut
		 */
		public function debut($tmp = 'debut', array $options = []){
			if(empty($options['title'])) {
				$options['title'] = "Page de test du FrameWork";
			}
			if (empty($options['application'])) {
				$options['application'] = 'Skatek Corporation';
			}
			$this->view->render($tmp, $options);
		}
		
		/**
		 * Pour rendre la vue 
		 */
		public function render($file = null, array $options = [])
		{
			return $this->view->render($file, $options);
		}
		
		/**
		 * Pour rediriger vers une page donner
		 */
		public function redirect($params = null)
		{
			return header('Location: ' . Router::buildUrl($params));
		}

		public static function buildUrl($params = null, $type = null)
		{
			return Router::buildUrl($params, $type);
		}

		public function e404(Type $var = null)
		{
			# code...
		}
		
		/**
		 * Fonction pour traquer les erreurs generer par CakeORM
		 * @param array $modelErrors Liste des errors
		 * @return array Erreus formates
		 */
		public function modelErrors(array $modelErrors = [])
		{
			$errorMessage = [];
            foreach($modelErrors as $errors){
                foreach($errors as $error) {
                    $errorMessage[] = $error;
                }
			}
			return $errorMessage;
		}

		/**
		 * Obtenir la view et ses parametres
		 */
		public function View() 		{ return $this->view; }
		public function Auth() 		{ return $this->auth; }
		public function Session()	{ return $this->session; }
		public function Request() 	{ return $this->request; }
		public function Flash($message = null, $type = "success") { $this->flash->$type($message); }

		/**
		 * Obtenir le model de l'application
		 * Vous pouvez changer la table en passant le nom de la table en parametre
		 * @param string $table Nom de la table a requeter
		 * @return Cake\ORM\TableRegistry L'objet table 
		 */
		public static function Model($table = null)
		{
			if ($table == null){
				return self::$model;
			}
			return TableRegistry::get($table, self::ModelClass($table));
		}

		/**
		 * Definition des className et de entityName pour l'ORM de type DATAMAPPER
		 * @param string $tableClass La classe a utiliser pour le Model
		 * @param string $entityClass La classe a utiliser pour l'entity
		 * @return array ['className', 'entityClass'] Les chemins complets du modelTable et de entityTable
		 */
		public static function ModelClass($tableClass = null)
		{
			if ($tableClass == null){
				$tableClass  = self::$className;
			}
			$tableClass = ucfirst($tableClass);
			
			$tabClass    = "App\Model\Table\\"  . $tableClass . "Table";
			$entityClass = "App\Model\Entity\\" . substr($tableClass, 0, -1);

			if (! class_exists($tabClass)) {
				$tabClass = null;
			}

			if (! class_exists($entityClass)) {
				$entityClass = null;
			}

			return [
				'className'   => $tabClass,
				'entityClass' => $entityClass
			];
		}

    /**
     * Detecte la presence d'une figure dans l'image
     * @param string $image L'image a verifier 
     * @return boolean Si la face a etet trouve ou non
     */
    public function faceDetect($image = null)
    {
        $data = CORE_CONFIG . DS . 'face_detection.dat';
        $detector = new FaceDetector($data);

        if (! is_file($image)){
            return false;
        }
        $detector->faceDetect($image);

        return is_array($detector->getFace());
	}
	
	public function generateUnique($n = null){
		if ($n == null) { return time(); }
        if ($n == 'min') { return sprintf('%03x-%03x', mt_rand(0, 65535), mt_rand(0, 65535)); }
        else if ($n == 'med') { return sprintf('%04x%04x%04x', date('Ym'), mt_rand(0, 65535), mt_rand(0, 65535)); }
        else { return sprintf('%04x%05x%05x', date('Ymd'), mt_rand(0, 65535), mt_rand(0, 65535)); }
    }
}