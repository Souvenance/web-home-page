<?php

namespace App\View;

use Stephanie\View\View;

class AppView extends View {
    public function __construct($class = null)
    {
        parent::__construct($class);

        //$this->addGlobal('app_theme'   , 'default');
        $this->addGlobal('software'    , 'Web File Explorer');
        $this->addGlobal('software_v'  , 'v1');
        $this->addGlobal('format_date' , 'l, d F Y');

        $this->addGlobal('R_AG', R_AG);
        $this->addGlobal('R_AD', R_AD);
        $this->addGlobal('R_GE', R_GE);
        $this->addGlobal('R_MA', R_MA);
    }
}