<?php

namespace App\Controller;

use Stephanie\Controller\Controller;

class AppController extends Controller {
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Permet de definir l'authorisation des users pour avoir certaines acces
     * @param integer $user_id ID de l'user
     * @return boolean|response
     */
    public function isAuthorized($user_id = null)
    {
        $user = $this->Auth()->getUser();

        if($user != null) {
            if (in_array($user['role'], [R_MA])){ return true; }

            if ($user['id'] == $user_id) { return true; }
        }

        $this->flash->error("Vous n'avez pas droit de venir ici.");
        return false; 
    }

}