<?php
/**
* Users.php
* @author Souvenance <skavunga@gmail.com>
* @version 1.1
* @importance Gestion des Utilisateurs
*/
namespace App\Controller;

use Cake\ORM\TableRegistry;

class Users extends AppController {

    /**
     * Connexion des utilisateurs:: Ouverture de session
     */
    public function connexion()
    {
        if ($this->request->is('post')){
            $data = $this->request->getData();
            
            $user = $this->auth->identify($data['username'], $data['password']);
            $url  = '/';
            if ($user){
                $this->auth->setUser($user);
                
                switch($user['role']){
                    case R_AD: $url = '/users'; break;
                    case R_GE: case R_MA: $url = '/entreprises/liste'; break;
                }
                return $this->redirect($url);
            }
            $this->flash->error("Informations incorrectes. Veuillez réesayer !");
            return $this->redirect('/');
        }
        $this->render('login', [
            'title' => 'Connexion'
        ]);
    }

    /**
     * Deconnecter un utilisateur
     */
    public function deconnexion()
    {
        $this->auth->logout();
        return $this->redirect('/');
    }

    /**
     * Affichage des informations de l'utilisateur
     * @param integer $id ID de l'utilisateur
     */
    public function profile($id = null)
    {
        if ($id == null){
            $id = $this->auth->getUser()['id'];
        }
        $this->isAuthorized($id);
        
        $user = $this->Model()->find()->where(['id' => $id])->first();

        $this->render('profile', [
            'title' => 'Profil de l\'agent',
            'user'  => $user
        ]);
    }

    /**
     * Lister touts les utilisateurs enregistrer
     */
    public function liste()
    {
        $this->isAuthorized();

        $users = $this->Model()->find('users');
        
        $this->render('liste', [
            'title' => 'Liste de tous les agents',
            'users' => $users
        ]);
    }

    /**
     * Ajout des administrateurs des entreprises
     */
    public function add_admin($entreprise = null)
    {
        $this->isAuthorized();

        $this->render('add_admin', [
            'title' => 'Ajouter un administrateur'
        ]);
    }

    /**
     * Creation d'un nouveau utilisateur
     */
    public function ajouter()
    {
        $this->isAuthorized();

        $user = $this->Model()->newEntity();

        if ($this->request->is('post')){
            $img   = $this->request->getData('photo');
            $datas = $this->request->getData();

            if ($img != NULL && file_exists(IMAGES_ROOT . $img)){
                $temp = \explode('.', $img);
                $datas['photo'] = 'user_' . $this->auth->getUser()['id'] . '_' . time() . '.' . end($temp);
                copy(IMAGES_ROOT . $img, IMAGES_ROOT . 'profile' . DS . $datas['photo']);
                unlink(IMAGES_ROOT . $img);
            } else {
                $datas['photo'] = NULL;
            }

            $user = $this->Model()->newEntity($datas);
            
            if ($this->Model()->save($user)) {
                $this->Model('historiques')->save($this->hEntity(null, $user->toArray()));
                return $this->redirect('/users');
            }            
            debugErrors($this->modelErrors($user->errors()));
        }

        /**
         * "ID de l'entreprise que l'on viens de creer"
         */
        $entreprise_id = $this->request->getQuery('entreprise');
        /**
         * Liste des entreprises a afficher dans la liste pour R_MA et R_GE
         */
        $entreprises = $this->Model('Entreprises')->find('list', [ 'valueFied' => 'name' ]);
        
        $this->render('add', [
            'title' => 'Ajouter un nouvel agent',
            'roles' => $this->getRoles(),
            'user'  => $user,
            'entreprises' => $entreprises,
            'entreprise_id' => $entreprise_id
        ]);
    }

    /**
     * Modification d'un utilisateur
     * Par lui meme ou par l'admin
     */
    public function modifier($id = null)
    {
        if ($id == null){
            $id = $this->auth->getUser()['id'];
        }

        $this->isAuthorized($id);

        $user = $this->Model()->find()->where(['id' => $id])->first();
        
        if ($this->request->is('post')){
            $img   = $this->request->getData('photo');
            $datas = $this->request->getData();

            if ($img != NULL && file_exists(IMAGES_ROOT . $img)){
                $temp = \explode('.', $img);
                
                $datas['photo'] = 'user_' . $this->auth->getUser()['id'] . '_' . time() . '.' . end($temp);
                copy(IMAGES_ROOT . $img, IMAGES_ROOT . 'profile' . DS . $datas['photo']);
                unlink(IMAGES_ROOT . $img);
            } else if($img == 'aucune') {
                $datas['photo'] = NULL;
            } else {
                $datas['photo'] = $user->photo;
            }
            
            $user = $this->Model()->patchEntity($user, $datas);

            if (empty($data['password'])){
                $user->setDirty('password',         false);
                $user->setDirty('confirm_password', false);
            }

            if(! in_array($this->auth->getUser()['role'], [R_AD, R_MA, R_GE])){
                ['username', 'role', 'fonction', 'active'];
                $user->setDirty('username', false);
                $user->setDirty('role',     false);
                $user->setDirty('fonction', false);
                $user->setDirty('active',   false);
            }

            if ($this->Model()->save($user)){
                $this->Model('historiques')->save($this->hEntity(null, $user->toArray()));
                $this->flash->success("Modifié avec succès.");
                return $this->redirect("Users#profile/" . $id);
            } 
            \debugErrors($this->modelErrors($user->errors()));
        }

        $entreprises = $this->Model('Entreprises')->find('list', [ 'valueFied' => 'name' ]);

        $this->render('edit', [
            'title' => 'Modifier ' . ucwords($user->nom . ' ' . $user->prenom),
            'user'  => $user,
            'roles' => $this->getRoles(),
            'entreprises' => $entreprises
        ]);
    }

    /**
     * Ajout et modification d'une photo de profil
     * @param int $id ID de l'user
     */
    public function photo($id = null)
    {
        // var_dump($this->session['router']);
        // exit;
        $profile = 'avatar.jpg';

        $this->render('photo', [
            'title'   => 'Photo',
            'profile' => $profile
        ]);
    }
    
    /**
     * Pour supprimer un utilisateur
     * Seul un admin peut supprimer un user
     * @param integer $id ID de user a supprimer
     */
    public function supprimer($id = null)
    {
        $this->isAuthorized();

        $user = $this->Model()->find()->where(['id' => $id])->first();

        if ($user == null) {
            $this->flash->error("Agent non identifié");
            return $this->redirect('/users');
        }

        if ($this->request->is('post')){
            if ($user != null) {
                $usr = $user;
                if ($this->Model()->delete($user)) {
                    $this->flash->success("Supprimé avec succès.");
                    $this->Model('historiques')->save($this->hEntity(null, $usr->toArray()));
                    return $this->redirect('/users');
                } 
                \debugErrors($this->modelErrors($user->errors()));
            }
        }
        
        $this->render('delete', [
            'title' => 'Supprimer un agent',
            'user'   => $user
        ]);
    }

    /**
     * La liste des roles des users
     * @return array|string|null
     */
    public function getRoles($key = null)
    {
        $role = $this->Auth()->getUser()['role'];
        $roles = [ R_AD => 'Administrateur', R_AG  => 'Utilisateur' ];
        if ($role == R_GE || $role == R_MA) {
            $roles += [ R_GE => 'Gestionnaire' ];
        }
        if ($role == R_MA) {
            $roles += [R_MA => 'Manager'];
        }

        if ($key == null) {
            return $roles;
        }
        return empty($roles[$key]) ? null : $roles[$key];
    }

    /**
     * Bloque access a certain users qui ne dipose pas de role necessaire
     */
    public function isAuthorized($id = null)
    {
        $action = $this->session['router']['action'];
        $user = $this->Auth()->getUser();
        if (in_array($user['role'], [R_AD, R_MA, R_GE])){
            return true;
        }
        if(! parent::isAuthorized($id)){
            return $this->redirect('/');
        }
    }
    
    /**
     * Enregistrement des images avant de les couper
     * utiliser par croppic
     */
    public function saveToFile()
    {
        if (! $this->request->is('post')){
            $response = [
                'status' => 'error',
                'message'=> 'Methode non permis'
            ];
            echo \json_encode($response);
            return;
        }
        $imagePath   = IMAGES_ROOT . "temp" . DS;
        $allowedExts = ["gif", "jpeg", "jpg", "png", "GIF", "JPEG", "JPG", "PNG"];
        $temp        = explode(".", $_FILES["img"]["name"]);
        $extension   = end($temp);

        if(!is_writable($imagePath)){
            $response = [
                "status"  => 'error',
                "message" => "Vous ne disposez pas des permission pour ecrire dans le dossier."
            ];
            print json_encode($response);
            return;
        }
        
        if (in_array($extension, $allowedExts)) {
            if ($_FILES["img"]["error"] > 0) {
                $response = array(
                    "status" => 'error',
                    "message" => 'ERROR Return Code: '. $_FILES["img"]["error"],
                );			
            } else {                
                $filename             = $_FILES["img"]["tmp_name"];
                list($width, $height) = getimagesize($filename);
                move_uploaded_file($filename, $imagePath . $_FILES["img"]["name"]);
                $response = [
                    "status" => 'success',
                    "url"    => IMAGES_TMP . $_FILES["img"]["name"],
                    "width"  => $width,
                    "height" => $height
                ];            
            }
        } else {
            $response = [
                "status"  => 'error',
                "message" => 'Quelque chose ne va pas! Veuillez réessayer de charger l\'image.',
            ];
        }
        
        print json_encode($response);
        return;
    }

    /**
     * Redimensionnement des images avec croppic
     */
    public function cropToFile()
    {
        if (! $this->request->is('post')){
            $response = [
                'status' => 'error',
                'message'=> 'Methode non permis'
            ];
            echo \json_encode($response);
            return;
        }
        
        $imgUrl   = $_POST['imgUrl'];
        // original sizes
        $imgInitW = $_POST['imgInitW'];
        $imgInitH = $_POST['imgInitH'];
        // resized sizes
        $imgW     = $_POST['imgW'];
        $imgH     = $_POST['imgH'];
        // offsets
        $imgY1    = $_POST['imgY1'];
        $imgX1    = $_POST['imgX1'];
        // cropbox
        $cropW    = $_POST['cropW'];
        $cropH    = $_POST['cropH'];
        // rotation angle
        $angle    = $_POST['rotation'];
        $imgUrl   = str_replace(IMAGES_TMP, IMAGES_ROOT . 'temp' . DS, $imgUrl);

        $jpeg_quality = 100;
        $nom_image    = 'skatek_crop_' . date('dmY_His');
        $output_filename = IMAGES_ROOT . "cropped" . DS . $nom_image;

        // uncomment line below to save the cropped image in the same location as the original image.
        // $output_filename = dirname($imgUrl). DS . $nom_image;// "/croppedImg_".rand();
        
        $what = getimagesize($imgUrl);

        switch(strtolower($what['mime']))
        {
            case 'image/png':
                $img_r        = imagecreatefrompng($imgUrl);
                $source_image = imagecreatefrompng($imgUrl);
                $type         = '.png';
                break;
            case 'image/jpeg':
                $img_r        = imagecreatefromjpeg($imgUrl);
                $source_image = imagecreatefromjpeg($imgUrl);
                $type         = '.jpeg';
                break;
            case 'image/gif':
                $img_r        = imagecreatefromgif($imgUrl);
                $source_image = imagecreatefromgif($imgUrl);
                $type         = '.gif';
                break;
            default: 
                die('Image type not supported');
        }

        if(! is_writable(dirname($output_filename))) {
            $response = [
                "status"  => 'error',
                "message" => "Vous ne disposez pas des permission pour enregistrer ce fichier dans le disque."
            ];	
        } else {

            // resize the original image to size of editor
            $resizedImage = imagecreatetruecolor($imgW, $imgH);
            imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
            // rotate the rezized image
            $rotated_image = imagerotate($resizedImage, -$angle, 0);
            // find new width & height of rotated image
            $rotated_width = imagesx($rotated_image);
            $rotated_height = imagesy($rotated_image);
            // diff between rotated & original sizes
            $dx = $rotated_width - $imgW;
            $dy = $rotated_height - $imgH;
            // crop rotated image to fit into original rezized rectangle
            $cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
            imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
            imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
            // crop image into selected area
            $final_image = imagecreatetruecolor($cropW, $cropH);
            imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
            imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
            // finally output png image
            //imagepng($final_image, $output_filename.$type, $png_quality);
            imagejpeg($final_image, $output_filename.$type, $jpeg_quality);
            $nom = 'cropped' . DS . $nom_image . $type;
            $response = [
                "status" => 'success',
                "url"    => IMAGES_DIR . $nom,
                "name"   => $nom
            ];
        }
        \unlink($imgUrl);
        print json_encode($response);
        exit;
    }
}